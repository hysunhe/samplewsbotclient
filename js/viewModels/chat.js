/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'jet-composites/bot-client/loader'],
        function (oj, ko, $) {

            function DashboardViewModel() {
                var self = this;
				// self.websocketConnectionUrl = 'ws://129.146.102.20:8888/chat/ws';
				//self.userId = 'User';

                var SO_SUFFIX = localStorage.getItem("SO_SUFFIX");
                if(!SO_SUFFIX) {
                    SO_SUFFIX = Math.ceil(Math.random() * 100000000);
                    localStorage.setItem("SO_SUFFIX", SO_SUFFIX);
                }
                console.log("*** SO_SUFFIX: " + SO_SUFFIX);

				self.channel = '9D8F7BC9-B745-426D-B81E-A58F208501BA';
				// self.channel = '2F3547CE-B0A9-4638-B6E7-9867E88B39A3';
                self.userId = 'HYS_' + SO_SUFFIX;

                self.firstName = 'HYS';
                self.lastName = SO_SUFFIX;
                //self.websocketConnectionUrl = 'wss://CPRWebSocketServer-pmomcenas2.uscom-central-1.oraclecloud.com/chat/ws';
                self.websocketConnectionUrl = 'wss://BDChatWebSocketServer-gse00015114.uscom-east-1.oraclecloud.com/chat/ws';
                // self.websocketConnectionUrl = 'wss://0744781c.ngrok.io/chat/ws';
				
				// // sso 
				// var value = "; " + document.cookie;
    //             //console.log("*** Cookie value=" + value);
    //             var parts = value.split("; " + "ORA_UCM_INFO" + "=");
    //             console.log("*** parts=", parts);
				// if (parts.length === 2) {
				// 	var arr = parts.pop().split(";").shift().split("~");
				// 	console.log(arr[2]);
				// 	console.log(arr[3]);
				// 	console.log(arr[4]);
				// 	self.firstName = arr[2];
				// 	self.lastName = arr[3];
				// 	self.userId = arr[4];
				// }

				self.fullName = ko.pureComputed(function() {
                    // Knockout tracks dependencies automatically. It knows that fullName depends on firstName and lastName, because these get called when evaluating fullName.
                    if(self.lastName && self.firstName !== self.lastName) {
                        return self.firstName + " " + self.lastName;
                    } else {
                        return self.firstName;
                    }
                }, this);

				// end sso
                // Below are a subset of the ViewModel methods invoked by the ojModule binding
                // Please reference the ojModule jsDoc for additional available methods.

                /**
                 * Optional ViewModel method invoked when this ViewModel is about to be
                 * used for the View transition.  The application can put data fetch logic
                 * here that can return a Promise which will delay the handleAttached function
                 * call below until the Promise is resolved.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
                 * the promise is resolved
                 */
                self.handleActivated = function (info) {
                    // Implement if needed
                };

                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
                 */
                self.handleAttached = function (info) {
                    // Implement if needed
                };


                /**
                 * Optional ViewModel method invoked after the bindings are applied on this View. 
                 * If the current View is retrieved from cache, the bindings will not be re-applied
                 * and this callback will not be invoked.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 */
                self.handleBindingsApplied = function (info) {
                    // Implement if needed
                };

                /*
                 * Optional ViewModel method invoked after the View is removed from the
                 * document DOM.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
                 */
                self.handleDetached = function (info) {
                    // Implement if needed
                };

                self.isMobileDev = ko.pureComputed(function() {
                    var ua = navigator.userAgent.toLowerCase();
                    var isAndroid = ua.indexOf("android") > -1;
                    if(isAndroid) {
                        return true;
                    } else {
                        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
                        if(iOS) {
                            return true;
                        } 
                    }
                    return false;
                });
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new DashboardViewModel();
        }
);
