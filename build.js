/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
({
    baseUrl: 'js',
    out: 'js/main-min.js',
    optimize: 'uglify2',
    name: 'main',
    paths: {
        'knockout': 'lib/knockout-3.4.0',
        'jquery': 'lib/jquery-3.1.1.min',
        'jqueryui-amd': 'lib/jqueryui-amd-1.12.0.min',
        'promise': 'lib/es6-promise.min',
        'hammerjs': 'lib/hammer-2.0.8.min',
        'ojs': 'lib/ojs',
        'ojL10n': 'lib/ojs/ojL10n',
        'ojtranslations': 'lib/ojs/ojtranslations',
        'text': 'lib/jqueryui-amd-1.12.0.min/text',
        'signals': 'lib/signals.min',
        'customElements': 'lib/custom-elements.min',
        'bot-client': 'jet-composites/bot-client'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'amplify': {
            deps: ['jquery'], exports: 'amplify'
        }
    }
}); 